package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Observer;

public class AnotherObserverForEventOfRequiredContext implements Observer<Context,EventOfRequiredContext> {
    @Override
    public void observe(EventOfRequiredContext event) {

    }
}
