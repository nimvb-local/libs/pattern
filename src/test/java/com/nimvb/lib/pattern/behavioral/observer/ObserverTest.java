package com.nimvb.lib.pattern.behavioral.observer;

import com.nimvb.lib.pattern.behavioral.observer.context.Context;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfEmptyContext;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfRequiredContext;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ObserverTest {


    class ObserverOfEmptyEvent implements Observer<Context, EventOfEmptyContext> {

        @Override
        public void observe(EventOfEmptyContext event) {

        }
    }

    class ObserverOfContext implements Observer<Context, EventOfRequiredContext> {

        @Override
        public void observe(EventOfRequiredContext event) {

        }
    }

}