package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Observer;

public class ObserverForEventOfMessage implements Observer<Context,EventOfMessage> {
    @Override
    public void observe(EventOfMessage event) {
        System.out.println("event of " + event.getClass().getName() +" is received!");
    }
}
