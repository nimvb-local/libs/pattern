package com.nimvb.lib.pattern.behavioral.observer.discovery;

import com.nimvb.lib.pattern.behavioral.observer.context.Context;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfEmptyContext;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfIgnorable;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfRequiredContext;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;

class ObserverBeanDefinitionContextBuilderTest {

    @Test
    void shouldReturnContextWithDesiredCommandsAndExecutors(){
        final ObserverBeanDefinitionContext.Builder builder = new ObserverBeanDefinitionContext.Builder(new String[]{Context.class.getPackageName()}, new SimpleBeanDefinitionRegistry());
        final ObserverBeanDefinitionContext context = builder.build();
        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(context.getSubjectsMap()).isNotNull();
        Assertions.assertThat(context.getObserversMap()).isNotNull();
        Assertions.assertThat(context.getSubjectsMap().containsKey(EventOfEmptyContext.class)).isTrue();
        Assertions.assertThat(context.getSubjectsMap().containsKey(EventOfRequiredContext.class)).isTrue();
        Assertions.assertThat(context.getSubjectsMap().containsKey(EventOfIgnorable.class)).isFalse();
        Assertions.assertThat(context.getObserversMap().containsKey(EventOfEmptyContext.class)).isTrue();
        Assertions.assertThat(context.getObserversMap().containsKey(EventOfRequiredContext.class)).isTrue();
        Assertions.assertThat(context.getObserversMap().containsKey(EventOfIgnorable.class)).isFalse();

    }

}