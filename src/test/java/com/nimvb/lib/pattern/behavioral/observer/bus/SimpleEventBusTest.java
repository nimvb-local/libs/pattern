package com.nimvb.lib.pattern.behavioral.observer.bus;

import com.nimvb.lib.pattern.behavioral.observer.context.Context;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.DefaultRegistryProvider;
import com.nimvb.lib.pattern.behavioral.observer.registry.RegistryProvider;
import com.nimvb.lib.pattern.behavioral.observer.registry.SimpleObserverRegistry;
import com.nimvb.lib.pattern.behavioral.observer.registry.SimpleSubjectRegistry;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class SimpleEventBusTest {


    @Autowired
    private ApplicationContext context;


    private ObserverBeanDefinitionContext buildDiscoveryContext() {
        return (
                new ObserverBeanDefinitionContext.Builder
                        (
                                new String[]
                                        {
                                                Context.class.getPackageName()
                                        },
                                new SimpleBeanDefinitionRegistry()
                        )
                        .build()
        );
    }


    @Test
    void shouldReturnFalseOnConnectedPartsStatus_WhenConstructedWithoutConnectedObservers(){
        RegistryProvider registryProvider = new DefaultRegistryProvider(new SimpleObserverRegistry(context,buildDiscoveryContext()),new SimpleSubjectRegistry(context,buildDiscoveryContext()));
        SimpleEventBus bus = new SimpleEventBus(registryProvider);
        Assertions.assertThat(bus).isNotNull();
        Assertions.assertThat(bus.isConnected()).isFalse();
    }

    @Test
    void shouldReturnCorrespondentConnectedPartsStatus_WhenConstructedWithExplicitConnectedObserversStatus(){
        RegistryProvider registryProvider = new DefaultRegistryProvider(new SimpleObserverRegistry(context,buildDiscoveryContext()),new SimpleSubjectRegistry(context,buildDiscoveryContext()));
        SimpleEventBus bus = new SimpleEventBus(registryProvider,true);
        SimpleEventBus disconnectedBus = new SimpleEventBus(registryProvider,false);
        Assertions.assertThat(bus).isNotNull();
        Assertions.assertThat(disconnectedBus).isNotNull();
        Assertions.assertThat(bus.isConnected()).isTrue();
        Assertions.assertThat(disconnectedBus.isConnected()).isFalse();
    }

}