package com.nimvb.lib.pattern.behavioral.observer.registry;

import com.nimvb.lib.pattern.behavioral.observer.context.Context;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class DefaultRegistryProviderTest {


    @Autowired
    private ApplicationContext context;


    private ObserverBeanDefinitionContext buildDiscoveryContext() {
        return (
                new ObserverBeanDefinitionContext.Builder
                        (
                                new String[]
                                        {
                                                Context.class.getPackageName()
                                        },
                                new SimpleBeanDefinitionRegistry()
                        )
                        .build()
        );
    }

    @Test
    void shouldReturnRegistryProvider_whenConstructingWithCorrectParameters() {
        SimpleObserverRegistry observerRegistry = new SimpleObserverRegistry(context, buildDiscoveryContext());
        SimpleSubjectRegistry subjectRegistry = new SimpleSubjectRegistry(context, buildDiscoveryContext());
        DefaultRegistryProvider registryProvider = new DefaultRegistryProvider(observerRegistry,subjectRegistry);
        Assertions.assertThat(registryProvider).isNotNull();
        Assertions.assertThat(registryProvider.getObserverRegistry()).isNotNull();
        Assertions.assertThat(registryProvider.getSubjectRegistry()).isNotNull();
        Assertions.assertThat(registryProvider.getSubjectRegistry()).isEqualTo(subjectRegistry);
        Assertions.assertThat(registryProvider.getObserverRegistry()).isEqualTo(observerRegistry);
    }

}