package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Observer;

public class ObserverForEventOfEmptyContext implements Observer<Context,EventOfEmptyContext> {
    @Override
    public void observe(EventOfEmptyContext event) {

    }
}
