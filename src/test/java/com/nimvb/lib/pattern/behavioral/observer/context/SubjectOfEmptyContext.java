package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Observer;
import com.nimvb.lib.pattern.behavioral.observer.Subject;

public class SubjectOfEmptyContext implements Subject<Context,EventOfEmptyContext> {


    @Override
    public void add(Observer<Context, EventOfEmptyContext> observer) {

    }

    @Override
    public void remove(Observer<Context, EventOfEmptyContext> observer) {

    }

    @Override
    public void notify(EventOfEmptyContext event) {

    }
}
