package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public
class EventOfRequiredContext implements Event<Context> {

    @NonNull
    private final Context context;

    @Override
    public Optional<Context> getContext() {
        return Optional.of(context);
    }
}
