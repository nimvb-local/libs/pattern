package com.nimvb.lib.pattern.behavioral.observer;

import com.nimvb.lib.pattern.behavioral.observer.context.Context;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfEmptyContext;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfRequiredContext;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class EventTest {

    @Test
    void shouldReturnNullableContext_whenEventOfEmptyContextDeclared(){
        EventOfEmptyContext eventOfEmptyContext = new EventOfEmptyContext();
        Assertions.assertThat(eventOfEmptyContext).isNotNull();
        Assertions.assertThat(eventOfEmptyContext.getContext().isEmpty()).isTrue();
    }

    @Test
    void shouldReturnContext_whenEventOfRequiredContextDeclared(){
        EventOfRequiredContext ofRequiredContext = new EventOfRequiredContext(new Context());
        Assertions.assertThat(ofRequiredContext).isNotNull();
        Assertions.assertThat(ofRequiredContext.getContext().isPresent()).isTrue();
    }

}