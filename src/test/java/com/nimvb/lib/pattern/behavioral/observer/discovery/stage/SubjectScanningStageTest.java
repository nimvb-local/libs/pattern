package com.nimvb.lib.pattern.behavioral.observer.discovery.stage;

import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import com.nimvb.lib.pattern.behavioral.observer.context.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;

class SubjectScanningStageTest {

    @Test
    void test(){
        String[] packages = new String[]{
                Context.class.getPackageName()
        };
        SubjectScanningStage scanningStage = new SubjectScanningStage(packages,new SimpleBeanDefinitionRegistry());
        StageContext context = new StageContext();
        context.setContent(new DiscoveryContext());
        context = scanningStage.execute(context, cntx -> {

        });
        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent())).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getSubjectsMap()).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getSubjectsMap().size() >= 2).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getSubjectsMap().keySet().stream().anyMatch(clazz -> clazz.equals(EventOfEmptyContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getSubjectsMap().keySet().stream().anyMatch(clazz -> clazz.equals(EventOfRequiredContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getSubjectsMap().values().stream().anyMatch(clazz -> clazz.equals(SubjectOfEmptyContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getSubjectsMap().values().stream().anyMatch(clazz -> clazz.equals(SubjectOfRequiredContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getSubjectsMap().get(EventOfEmptyContext.class).equals(SubjectOfEmptyContext.class)).isTrue();
    }

}