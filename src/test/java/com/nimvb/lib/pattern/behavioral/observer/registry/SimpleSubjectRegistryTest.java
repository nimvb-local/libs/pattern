package com.nimvb.lib.pattern.behavioral.observer.registry;


import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Subject;
import com.nimvb.lib.pattern.behavioral.observer.context.Context;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfIgnorable;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfRequiredContext;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.exception.EventTypeNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
class SimpleSubjectRegistryTest {

    @Autowired
    private ApplicationContext context;


    private ObserverBeanDefinitionContext buildDiscoveryContext() {
        return (
                new ObserverBeanDefinitionContext.Builder
                        (
                                new String[]
                                        {
                                                Context.class.getPackageName()
                                        },
                                new SimpleBeanDefinitionRegistry()
                        )
                        .build()
        );
    }

    @Test
    void shouldReturnRegistry_whenConstructingWithCorrectParameters() {
        SimpleSubjectRegistry registry = new SimpleSubjectRegistry(context, buildDiscoveryContext());
        Assertions.assertThat(registry).isNotNull();
    }

    @Test
    void shouldReturnSubject_whenTheCorrespondentEventIsProvided() {
        SimpleSubjectRegistry registry = new SimpleSubjectRegistry(context, buildDiscoveryContext());
        final Subject<Context, EventOfRequiredContext> subject = registry.getSubjectOfEvent(EventOfRequiredContext.class);
        Assertions.assertThat(subject).isNotNull();
    }

    @Test
    void shouldReturnSubject_whenTheCorrespondentEventIsProvidedAndTheExplicitSubjectIsNotProvided() {
        SimpleSubjectRegistry registry = new SimpleSubjectRegistry(context, buildDiscoveryContext());
        final Subject<Context, EventOfIgnorable> subject = registry.getSubjectOfEvent(EventOfIgnorable.class);
        Assertions.assertThat(subject).isNotNull();
    }

    @Test
    void shouldReturnSameSubject_whenTheCorrespondentEventIsProvidedAndTheExplicitSubjectIsNotProvidedAndCalledMultipleTimes() {
        SimpleSubjectRegistry registry = new SimpleSubjectRegistry(context, buildDiscoveryContext());
        final Subject<Context, EventOfIgnorable> subject = registry.getSubjectOfEvent(EventOfIgnorable.class);
        final Subject<Context, EventOfIgnorable> another = registry.getSubjectOfEvent(EventOfIgnorable.class);
        Assertions.assertThat(subject).isNotNull();
        Assertions.assertThat(another).isNotNull();
        Assertions.assertThat(subject).isEqualTo(another);
    }


    @Test
    void shouldReturnSameSubject_whenTheCorrespondentEventIsProvidedAndCalledMultipleTimes() {
        SimpleSubjectRegistry registry = new SimpleSubjectRegistry(context, buildDiscoveryContext());
        final Subject<Context, EventOfRequiredContext> subject = registry.getSubjectOfEvent(EventOfRequiredContext.class);
        final Subject<Context, EventOfRequiredContext> another = registry.getSubjectOfEvent(EventOfRequiredContext.class);
        Assertions.assertThat(subject).isNotNull();
        Assertions.assertThat(another).isNotNull();
        Assertions.assertThat(subject).isEqualTo(another);
    }


    @Test
    void shouldThrowException_whenTheInvalidEventIsProvided() {
        SimpleSubjectRegistry registry = new SimpleSubjectRegistry(context, buildDiscoveryContext());
        Assertions.assertThatExceptionOfType(EventTypeNotFoundException.class).isThrownBy(() -> {
            Event<Object> ignoredEvent = Optional::empty;
            final Subject subject = registry.getSubjectOfEvent(ignoredEvent.getClass());
        });
    }

    @Test
    void shouldThrowException_whenTheNULLEventIsProvided() {
        SimpleSubjectRegistry registry = new SimpleSubjectRegistry(context, buildDiscoveryContext());
        Assertions.assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> {
            final Subject subject = registry.getSubjectOfEvent(null);
        });
    }

}