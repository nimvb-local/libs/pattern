package com.nimvb.lib.pattern.behavioral.chain;

import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChainTest {
    @Test
    void test() {
        Object content = Chain.INSTANCE.builder()
                .add((context, chain) -> {
                    context.setContent(1);
                    System.out.println("Hello stage 1");
                    chain.next(context);
                    return context;
                })

                .add((context, chain) -> {
                    System.out.println("Hello stage 2");
                    context.setContent(((int) context.getContent()) + 10);
                    chain.next(context);
                    return context;
                })
                .build()
                .execute(new StageContext())
                .content();
        int g = 0;
    }
}