package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Observer;

public class ObserverForEventOfRequiredContext implements Observer<Context,EventOfRequiredContext> {
    @Override
    public void observe(EventOfRequiredContext event) {
        System.out.println("event received");
    }
}
