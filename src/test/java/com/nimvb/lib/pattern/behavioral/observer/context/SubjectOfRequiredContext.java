package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Observer;
import com.nimvb.lib.pattern.behavioral.observer.Subject;

public class SubjectOfRequiredContext implements Subject<Context,EventOfRequiredContext> {


    @Override
    public void add(Observer<Context, EventOfRequiredContext> observer) {

    }

    @Override
    public void remove(Observer<Context, EventOfRequiredContext> observer) {

    }

    @Override
    public void notify(EventOfRequiredContext event) {

    }
}
