package com.nimvb.lib.pattern.behavioral.observer.discovery.stage;

import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import com.nimvb.lib.pattern.behavioral.observer.context.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;

import static org.junit.jupiter.api.Assertions.*;

class ObserverScanningStageTest {

    @Test
    void test(){
        String[] packages = new String[]{
                Context.class.getPackageName()
        };
        ObserverScanningStage scanningStage = new ObserverScanningStage(packages,new SimpleBeanDefinitionRegistry());
        StageContext context = new StageContext();
        context.setContent(new DiscoveryContext());
        context = scanningStage.execute(context, cntx -> {

        });
        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent())).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap()).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().size() >= 2).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().keySet().stream().anyMatch(clazz -> clazz.equals(EventOfEmptyContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().keySet().stream().anyMatch(clazz -> clazz.equals(EventOfRequiredContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().values().size() >= 2).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().get(EventOfRequiredContext.class).size() >= 2).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().get(EventOfEmptyContext.class).size() >= 1).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().get(EventOfEmptyContext.class).stream().anyMatch(clazz -> clazz.equals(ObserverForEventOfEmptyContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().get(EventOfRequiredContext.class).stream().anyMatch(clazz -> clazz.equals(ObserverForEventOfRequiredContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getObserversMap().get(EventOfRequiredContext.class).stream().anyMatch(clazz -> clazz.equals(AnotherObserverForEventOfRequiredContext.class))).isTrue();
    }

}