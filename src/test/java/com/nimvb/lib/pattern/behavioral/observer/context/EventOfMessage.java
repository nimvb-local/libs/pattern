package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class EventOfMessage implements Event<Context> {
    @Getter
    private final String message;

    @Override
    public Optional<Context> getContext() {
        return Optional.empty();
    }
}
