package com.nimvb.lib.pattern.behavioral.observer.bus;

import com.nimvb.lib.pattern.behavioral.observer.context.Context;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashMap;

@ExtendWith(SpringExtension.class)
class SimpleConnectorTest {

    @Autowired
    private ApplicationContext context;

    private ObserverBeanDefinitionContext buildDiscoveryContext() {
        return (
                new ObserverBeanDefinitionContext.Builder
                        (
                                new String[]
                                        {
                                                Context.class.getPackageName()
                                        },
                                new SimpleBeanDefinitionRegistry()
                        )
                        .build()
        );
    }

    @Test
    void shouldReturnEventbus_WhenTheRequiredConstructorParametersProvided(){
        RegistryProvider registryProvider = new DefaultRegistryProvider(new SimpleObserverRegistry(context,buildDiscoveryContext()),new SimpleSubjectRegistry(context,buildDiscoveryContext(),null));
        SimpleConnector connector = new SimpleConnector(buildDiscoveryContext(),registryProvider);
        final EventBus eventBus = connector.build();
        Assertions.assertThat(eventBus).isNotNull();
    }

    @Test
    void shouldThrowException_WhenTheNULLDefinitionContextIsProvided(){
        Assertions.assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> {
            SimpleConnector connector = new SimpleConnector(null, new RegistryProvider() {
                @Override
                public ObserverRegistry getObserverRegistry() {
                    return null;
                }

                @Override
                public SubjectRegistry getSubjectRegistry() {
                    return null;
                }
            });
        });
    }

    @Test
    void shouldThrowException_WhenTheNULLRegistryProviderIsProvided(){
        Assertions.assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> {
            SimpleConnector connector = new SimpleConnector(new ObserverBeanDefinitionContext(new HashMap<>(),new HashMap<>(),new ArrayList<>()), null);
        });
    }

}