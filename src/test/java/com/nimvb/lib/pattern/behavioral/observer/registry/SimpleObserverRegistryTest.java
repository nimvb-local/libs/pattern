package com.nimvb.lib.pattern.behavioral.observer.registry;

import com.nimvb.lib.pattern.behavioral.observer.Observer;
import com.nimvb.lib.pattern.behavioral.observer.Subject;
import com.nimvb.lib.pattern.behavioral.observer.context.Context;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfIgnorable;
import com.nimvb.lib.pattern.behavioral.observer.context.EventOfRequiredContext;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.exception.EventTypeNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class SimpleObserverRegistryTest {

    @Autowired
    private ApplicationContext context;


    private ObserverBeanDefinitionContext buildDiscoveryContext() {
        return (
                new ObserverBeanDefinitionContext.Builder
                        (
                                new String[]
                                        {
                                                Context.class.getPackageName()
                                        },
                                new SimpleBeanDefinitionRegistry()
                        )
                        .build()
        );
    }


    @Test
    void shouldReturnRegistry_whenConstructingWithCorrectParameters() {
        SimpleObserverRegistry registry = new SimpleObserverRegistry(context, buildDiscoveryContext());
        Assertions.assertThat(registry).isNotNull();
    }

    @Test
    void shouldReturnObservers_whenTheCorrespondentEventIsProvided() {
        SimpleObserverRegistry registry = new SimpleObserverRegistry(context, buildDiscoveryContext());
        final List<Observer<Context, EventOfRequiredContext>> observers = registry.getObserversOfEvent(EventOfRequiredContext.class);
        Assertions.assertThat(observers).isNotNull();
        Assertions.assertThat(observers).isInstanceOf(List.class);
        Assertions.assertThat(observers).hasSizeGreaterThanOrEqualTo(2);
    }

    @Test
    void shouldReturnSameObservers_whenTheCorrespondentEventIsProvidedAndCalledMultipleTimes() {
        SimpleObserverRegistry registry = new SimpleObserverRegistry(context, buildDiscoveryContext());
        final List<Observer<Context, EventOfRequiredContext>> observers = registry.getObserversOfEvent(EventOfRequiredContext.class);
        final List<Observer<Context, EventOfRequiredContext>> anotherListOfObservers = registry.getObserversOfEvent(EventOfRequiredContext.class);
        Assertions.assertThat(observers).isNotNull();
        Assertions.assertThat(anotherListOfObservers).isNotNull();
        Assertions.assertThat(observers).hasSameSizeAs(anotherListOfObservers);
        Assertions.assertThat(observers).isEqualTo(anotherListOfObservers);
    }

    @Test
    void shouldThrowException_whenTheInvalidEventIsProvided() {
        SimpleObserverRegistry registry = new SimpleObserverRegistry(context, buildDiscoveryContext());
        Assertions.assertThatExceptionOfType(EventTypeNotFoundException.class).isThrownBy(() -> {
            final List observers = registry.getObserversOfEvent(EventOfIgnorable.class);
        });
    }

    @Test
    void shouldThrowException_whenTheNULLEventIsProvided() {
        SimpleObserverRegistry registry = new SimpleObserverRegistry(context, buildDiscoveryContext());
        Assertions.assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> {
            final List subject = registry.getObserversOfEvent(null);
        });
    }


}