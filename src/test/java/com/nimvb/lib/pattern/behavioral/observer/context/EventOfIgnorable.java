package com.nimvb.lib.pattern.behavioral.observer.context;

import com.nimvb.lib.pattern.behavioral.observer.Event;

import java.util.Optional;

public class EventOfIgnorable implements Event<Context> {
    @Override
    public Optional<Context> getContext() {
        return Optional.empty();
    }
}
