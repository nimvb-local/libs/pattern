package com.nimvb.lib.pattern.behavioral.observer.discovery.stage;

import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import com.nimvb.lib.pattern.behavioral.observer.context.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;

import static org.junit.jupiter.api.Assertions.*;

class EventScanningStageTest {

    @Test
    void test(){
        String[] packages = new String[]{
                Context.class.getPackageName()
        };
        EventScanningStage scanningStage = new EventScanningStage(packages,new SimpleBeanDefinitionRegistry());
        StageContext context = new StageContext();
        context.setContent(new DiscoveryContext());
        context = scanningStage.execute(context, cntx -> {

        });
        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent())).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getEvents()).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getEvents().size() >= 3).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getEvents().stream().anyMatch(clazz -> clazz.equals(EventOfEmptyContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getEvents().stream().anyMatch(clazz -> clazz.equals(EventOfRequiredContext.class))).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getEvents().stream().anyMatch(clazz -> clazz.equals(EventOfIgnorable.class))).isTrue();
    }

}