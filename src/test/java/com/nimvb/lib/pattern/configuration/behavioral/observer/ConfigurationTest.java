package com.nimvb.lib.pattern.configuration.behavioral.observer;


import com.nimvb.lib.pattern.LibraryTestConfiguration;
import com.nimvb.lib.pattern.behavioral.observer.async.EnableAsyncObserver;
import com.nimvb.lib.pattern.behavioral.observer.async.ObserverAsyncConfiguration;
import com.nimvb.lib.pattern.behavioral.observer.bus.EventBus;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.RegistryProvider;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

//@EnableAsyncObserver
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = LibraryTestConfiguration.class)
@TestPropertySource(locations = {"classpath:application-test.yaml"},
        properties = {"spring.patterns.behavioral.observer.packages=com.nimvb.lib.pattern.behavioral.*"})
public class ConfigurationTest {

    @Autowired
    private ObserverBeanDefinitionContext context;

    @Autowired
    private RegistryProvider registryProvider;

    @Autowired
    private EventBus eventBus;

//    @Autowired
//    private ObserverAsyncConfiguration configuration;

    @Test
    void shouldReturnAutoWiredProperties_whenContextHasBeenLoaded() {

        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(registryProvider).isNotNull();
        Assertions.assertThat(eventBus).isNotNull();
        //Assertions.assertThat(configuration).isNotNull();
    }
}
