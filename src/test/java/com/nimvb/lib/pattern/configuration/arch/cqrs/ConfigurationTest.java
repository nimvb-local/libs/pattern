package com.nimvb.lib.pattern.configuration.arch.cqrs;

import com.nimvb.lib.pattern.LibraryTestConfiguration;
import com.nimvb.lib.pattern.arch.cqrs.bus.BusProvider;
import com.nimvb.lib.pattern.arch.cqrs.registry.RegistryProvider;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.CqrsBeansDefinitionContext;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = LibraryTestConfiguration.class)
@TestPropertySource(locations = {"classpath:application-test.yaml"},
        properties = {"spring.patterns.cqrs.packages=com.nimvb.lib.pattern.arch.*"})
class ConfigurationTest {

    @Autowired
    private CqrsBeansDefinitionContext context;

    @Autowired
    private RegistryProvider registryProvider;

    @Autowired
    private BusProvider busProvider;

    @Test
    void shouldReturnAutoWiredProperties_whenContextHasBeenLoaded() {
        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(registryProvider).isNotNull();
        Assertions.assertThat(busProvider).isNotNull();
    }
}