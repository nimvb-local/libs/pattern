package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.command;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;

public class CommandWithNoExplicitType implements Command {
}
