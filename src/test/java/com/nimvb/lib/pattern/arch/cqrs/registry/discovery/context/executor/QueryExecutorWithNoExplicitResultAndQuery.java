package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.executor;

import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;

public class QueryExecutorWithNoExplicitResultAndQuery implements QueryExecutor {
    @Override
    public Object execute(Query query) {
        return new Object();
    }
}
