package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.executor;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;
import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;

public class CommandExecutorWithNoExplicitResultAndCommand implements CommandExecutor {
    @Override
    public Object execute(Command command) {
        return new Object();
    }
}
