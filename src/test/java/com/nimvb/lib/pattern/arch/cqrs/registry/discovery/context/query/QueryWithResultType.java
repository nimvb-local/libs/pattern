package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.query;

import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import lombok.Getter;

@Getter
public class QueryWithResultType implements Query<ResultType> {
}
