package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.stage;

import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.DiscoveryContext;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.command.CommandWithResultType;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;

class CommandExecutorsScanningStageTest {

    @Test
    public void test() {
        String[] packages = new String[]{
                ResultType.class.getPackageName()
        };
        CommandExecutorsScanningStage scanningStage = new CommandExecutorsScanningStage(packages,new SimpleBeanDefinitionRegistry());
        StageContext context = new StageContext();
        context.setContent(new DiscoveryContext());
        context = scanningStage.execute(context, cntx -> {

        });
        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent())).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getCommandExecutorMap()).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getCommandExecutorMap().size() == 1).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getCommandExecutorMap().get(CommandWithResultType.class)).isNotNull();
    }

}