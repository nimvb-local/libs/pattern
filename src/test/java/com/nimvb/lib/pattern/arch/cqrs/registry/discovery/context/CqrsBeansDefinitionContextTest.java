package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CqrsBeansDefinitionContextTest {

    @Test
    void shouldReturnEmptyCommandExecutorMap_WhenNewContextCreated() {
        CqrsBeansDefinitionContext context = new CqrsBeansDefinitionContext();
        Assertions.assertThat(context.getCommandExecutorMap()).isNotNull();
        Assertions.assertThat(context.getCommandExecutorMap().size() == 0).isTrue();
    }


    @Test
    void shouldThrowException_WhenNullPassedAsExecutorMap(){
        assertThrows(Exception.class,() -> {
            CqrsBeansDefinitionContext context = new CqrsBeansDefinitionContext(null,null);
        });
    }

}