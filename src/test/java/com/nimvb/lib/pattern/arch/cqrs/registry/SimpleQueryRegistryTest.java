package com.nimvb.lib.pattern.arch.cqrs.registry;

import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.CqrsBeansDefinitionContext;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.query.QueryWithNoExplicitType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.query.QueryWithResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.exception.QueryExecutorNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class SimpleQueryRegistryTest {

    @Autowired
    private ApplicationContext context;


    private CqrsBeansDefinitionContext buildDiscoveryContext() {
        return (
                new CqrsBeansDefinitionContext.Builder
                        (
                                new String[]
                                        {
                                                SimpleQueryRegistry.class.getPackageName()
                                        },
                                new SimpleBeanDefinitionRegistry()
                        )
                        .build()
        );
    }

    @Test
    void shouldReturnRegistry_whenConstructingWithCorrectParameters() {
        SimpleQueryRegistry registry = new SimpleQueryRegistry(context, buildDiscoveryContext());
        Assertions.assertThat(registry).isNotNull();
    }

    @Test
    void shouldReturnExecutor_whenTheCorrespondentQueryIsProvided() {
        SimpleQueryRegistry registry = new SimpleQueryRegistry(context, buildDiscoveryContext());
        final QueryExecutor<ResultType, QueryWithResultType> executor = registry.getExecutor(QueryWithResultType.class);
        Assertions.assertThat(executor).isNotNull();
    }


    @Test
    void shouldReturnSameExecutor_whenTheCorrespondentQueryIsProvidedAndCalledMultipleTimes() {
        SimpleQueryRegistry registry = new SimpleQueryRegistry(context, buildDiscoveryContext());
        final QueryExecutor<ResultType, QueryWithResultType> executor = registry.getExecutor(QueryWithResultType.class);
        final QueryExecutor<ResultType, QueryWithResultType> another = registry.getExecutor(QueryWithResultType.class);
        Assertions.assertThat(executor).isNotNull();
        Assertions.assertThat(another).isNotNull();
        Assertions.assertThat(executor).isEqualTo(another);
    }


    @Test
    void shouldThrowException_whenTheInvalidQueryIsProvided() {
        SimpleQueryRegistry registry = new SimpleQueryRegistry(context, buildDiscoveryContext());
        Assertions.assertThatExceptionOfType(QueryExecutorNotFoundException.class).isThrownBy(() -> {
            final QueryExecutor executor = registry.getExecutor(QueryWithNoExplicitType.class);
        });
    }

    @Test
    void shouldThrowException_whenTheNULLQueryIsProvided() {
        SimpleQueryRegistry registry = new SimpleQueryRegistry(context, buildDiscoveryContext());
        Assertions.assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> {
            final QueryExecutor executor = registry.getExecutor(null);
        });
    }

}