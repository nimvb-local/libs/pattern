package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.executor;

import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.query.QueryWithResultType;

public class QueryExecutorWithResult  implements QueryExecutor<ResultType, QueryWithResultType> {

    @Override
    public ResultType execute(QueryWithResultType query) {
        return new ResultType();
    }
}
