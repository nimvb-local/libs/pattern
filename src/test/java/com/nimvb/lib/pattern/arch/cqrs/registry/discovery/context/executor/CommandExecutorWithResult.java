package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.executor;

import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.command.CommandWithResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CommandExecutorWithResult implements CommandExecutor<ResultType, CommandWithResultType> {

    @Override
    public ResultType execute(CommandWithResultType command) {
        return new ResultType();
    }
}
