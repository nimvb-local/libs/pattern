package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.stage;

import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.DiscoveryContext;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.query.QueryWithResultType;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;

class QueryExecutorsScanningStageTest {

    @Test
    void test(){
        String[] packages = new String[]{
                ResultType.class.getPackageName()
        };
        QueryExecutorsScanningStage scanningStage = new QueryExecutorsScanningStage(packages,new SimpleBeanDefinitionRegistry());
        StageContext context = new StageContext();
        context.setContent(new DiscoveryContext());
        context = scanningStage.execute(context, cntx -> {

        });
        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent())).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getQueryExecutorMap()).isNotNull();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getQueryExecutorMap().size() == 1).isTrue();
        Assertions.assertThat(((DiscoveryContext) context.getContent()).getQueryExecutorMap().get(QueryWithResultType.class)).isNotNull();
    }

}