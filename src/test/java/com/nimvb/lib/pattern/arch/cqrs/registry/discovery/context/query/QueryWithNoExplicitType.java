package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.query;

import com.nimvb.lib.pattern.arch.cqrs.query.Query;

public class QueryWithNoExplicitType implements Query {
}
