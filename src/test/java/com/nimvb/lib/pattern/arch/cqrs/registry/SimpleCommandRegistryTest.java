package com.nimvb.lib.pattern.arch.cqrs.registry;

import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.CqrsBeansDefinitionContext;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.command.CommandWithNoExplicitType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.command.CommandWithResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.exception.CommandExecutorNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class SimpleCommandRegistryTest {

    @Autowired
    private ApplicationContext context;


    private CqrsBeansDefinitionContext buildDiscoveryContext() {
        return (
                new CqrsBeansDefinitionContext.Builder
                        (
                                new String[]
                                        {
                                                SimpleCommandRegistry.class.getPackageName()
                                        },
                                new SimpleBeanDefinitionRegistry()
                        )
                        .build()
        );
    }

    @Test
    void shouldReturnRegistry_whenConstructingWithCorrectParameters() {
        SimpleCommandRegistry registry = new SimpleCommandRegistry(context, buildDiscoveryContext());
        Assertions.assertThat(registry).isNotNull();
    }

    @Test
    void shouldReturnExecutor_whenTheCorrespondentCommandIsProvided() {
        SimpleCommandRegistry registry = new SimpleCommandRegistry(context, buildDiscoveryContext());
        final CommandExecutor<ResultType, CommandWithResultType> executor = registry.getExecutor(CommandWithResultType.class);
        Assertions.assertThat(executor).isNotNull();
    }


    @Test
    void shouldReturnSameExecutor_whenTheCorrespondentCommandIsProvidedAndCalledMultipleTimes() {
        SimpleCommandRegistry registry = new SimpleCommandRegistry(context, buildDiscoveryContext());
        final CommandExecutor<ResultType, CommandWithResultType> executor = registry.getExecutor(CommandWithResultType.class);
        final CommandExecutor<ResultType, CommandWithResultType> another = registry.getExecutor(CommandWithResultType.class);
        Assertions.assertThat(executor).isNotNull();
        Assertions.assertThat(another).isNotNull();
        Assertions.assertThat(executor).isEqualTo(another);
    }


    @Test
    void shouldThrowException_whenTheInvalidCommandIsProvided() {
        SimpleCommandRegistry registry = new SimpleCommandRegistry(context, buildDiscoveryContext());
        Assertions.assertThatExceptionOfType(CommandExecutorNotFoundException.class).isThrownBy(() -> {
            final CommandExecutor executor = registry.getExecutor(CommandWithNoExplicitType.class);
        });
    }

    @Test
    void shouldThrowException_whenTheNULLCommandIsProvided() {
        SimpleCommandRegistry registry = new SimpleCommandRegistry(context, buildDiscoveryContext());
        Assertions.assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> {
            final CommandExecutor executor = registry.getExecutor(null);
        });
    }
}