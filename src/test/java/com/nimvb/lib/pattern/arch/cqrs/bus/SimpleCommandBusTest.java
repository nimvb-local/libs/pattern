package com.nimvb.lib.pattern.arch.cqrs.bus;

import com.nimvb.lib.pattern.LibraryTestConfiguration;
import com.nimvb.lib.pattern.arch.cqrs.registry.RegistryProvider;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.command.CommandWithResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = LibraryTestConfiguration.class)
@TestPropertySource(locations = {"classpath:application-test.yaml"},
        properties = {"spring.patterns.cqrs.packages=com.nimvb.lib.pattern.arch.*"})
class SimpleCommandBusTest {

    @Autowired
    private RegistryProvider provider;


    @Test
    void shouldReturnResult_whenCommandBusExecutedTheCorrespondentCommand(){
        SimpleCommandBus bus = new SimpleCommandBus(provider.getCommandRegistry());
        final ResultType resultType = bus.execute(new CommandWithResultType());
        org.assertj.core.api.Assertions.assertThat(resultType).isNotNull();
    }

}