package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.command;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.ResultType;
import lombok.Getter;



@Getter
public class CommandWithResultType implements Command<ResultType> {


}
