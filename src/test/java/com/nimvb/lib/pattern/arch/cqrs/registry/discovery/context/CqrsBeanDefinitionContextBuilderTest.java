package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context;

import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.command.CommandWithResultType;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.query.QueryWithResultType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;

class CqrsBeanDefinitionContextBuilderTest {

    @Test
    void shouldReturnContextWithDesiredCommandsAndExecutors(){
        final CqrsBeansDefinitionContext.Builder builder = new CqrsBeansDefinitionContext.Builder(new String[]{ResultType.class.getPackageName()}, new SimpleBeanDefinitionRegistry());
        final CqrsBeansDefinitionContext context = builder.build();
        Assertions.assertThat(context).isNotNull();
        Assertions.assertThat(context.getCommandExecutorMap()).isNotNull();
        Assertions.assertThat(context.getQueryExecutorMap()).isNotNull();
        Assertions.assertThat(context.getCommandExecutorMap().containsKey(CommandWithResultType.class)).isTrue();
        Assertions.assertThat(context.getCommandExecutorMap().containsKey(QueryWithResultType.class)).isFalse();
        Assertions.assertThat(context.getQueryExecutorMap().containsKey(QueryWithResultType.class)).isTrue();
        Assertions.assertThat(context.getQueryExecutorMap().containsKey(CommandWithResultType.class)).isFalse();

    }

}