package com.nimvb.lib.pattern;


import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@EnableAutoConfiguration
@SpringBootConfiguration
public class LibraryTestConfiguration {
}
