package com.nimvb.lib.pattern.configuration.arch.cqrs;

import com.nimvb.lib.pattern.arch.cqrs.bus.BusProvider;
import com.nimvb.lib.pattern.arch.cqrs.bus.CommandBus;
import com.nimvb.lib.pattern.arch.cqrs.registry.RegistryProvider;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(CommandBus.class)
@AutoConfigureAfter(RegistryAutoConfiguration.class)
public class BusAutoConfiguration {


    @Bean
    BusProvider busProvider(RegistryProvider provider){
        return new BusProvider.DefaultBusProvider(provider);
    }

}
