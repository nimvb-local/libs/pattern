package com.nimvb.lib.pattern.configuration.arch.cqrs;

import com.nimvb.lib.pattern.arch.cqrs.registry.*;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.CqrsBeansDefinitionContext;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(RegistryProvider.class)
@AutoConfigureAfter(CqrsBeanDefinitionAutoConfiguration.class)
public class RegistryAutoConfiguration {

    @Bean
    RegistryProvider registryProvider(ApplicationContext context, CqrsBeansDefinitionContext cqrsBeansDefinitionContext) {
        return new RegistryProvider.DefaultRegistryProvider(commandRegistry(context, cqrsBeansDefinitionContext), queryRegistry(context, cqrsBeansDefinitionContext));
    }

    private CommandRegistry commandRegistry(ApplicationContext context, CqrsBeansDefinitionContext cqrsBeansDefinitionContext) {
        return new SimpleCommandRegistry(context, cqrsBeansDefinitionContext);
    }

    private QueryRegistry queryRegistry(ApplicationContext context, CqrsBeansDefinitionContext cqrsBeansDefinitionContext) {
        return new SimpleQueryRegistry(context,cqrsBeansDefinitionContext);
    }
}
