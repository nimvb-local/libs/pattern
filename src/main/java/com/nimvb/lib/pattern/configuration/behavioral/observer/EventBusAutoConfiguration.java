package com.nimvb.lib.pattern.configuration.behavioral.observer;


import com.nimvb.lib.pattern.behavioral.observer.bus.Connector;
import com.nimvb.lib.pattern.behavioral.observer.bus.EventBus;
import com.nimvb.lib.pattern.behavioral.observer.bus.SimpleConnector;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.RegistryProvider;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass({Connector.class, SimpleConnector.class})
@AutoConfigureAfter(RegistryAutoConfiguration.class)
public class EventBusAutoConfiguration {

    @Bean
    EventBus eventBus(ObserverBeanDefinitionContext context, RegistryProvider registryProvider) {
        return new SimpleConnector(context, registryProvider).build();
    }
}
