package com.nimvb.lib.pattern.configuration.behavioral.observer;

import com.nimvb.lib.pattern.behavioral.observer.async.ObserverAsyncConfiguration;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.*;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(RegistryProvider.class)
@AutoConfigureAfter(ObserverBeanDefinitionContextAutoConfiguration.class)
public class RegistryAutoConfiguration {

    @ConditionalOnMissingBean(value = {ObserverAsyncConfiguration.class})
    @Bean
    RegistryProvider registryProviderOfObserverPattern(ApplicationContext context, ObserverBeanDefinitionContext definitionContext) {
        return new DefaultRegistryProvider(createObserverRegistry(context,definitionContext),createSubjectRegistry(context,definitionContext));
    }

    @ConditionalOnBean(value = {ObserverAsyncConfiguration.class})
    @Bean
    RegistryProvider registryProviderOfObserverPatternAsync(ApplicationContext context, ObserverBeanDefinitionContext definitionContext,ObserverAsyncConfiguration observerAsyncConfiguration) {
        return new DefaultRegistryProvider(createObserverRegistry(context,definitionContext),createSubjectRegistry(context,definitionContext,observerAsyncConfiguration));
    }

    private SubjectRegistry createSubjectRegistry(ApplicationContext context, ObserverBeanDefinitionContext definitionContext) {
        return createSubjectRegistry(context,definitionContext,null);
    }

    private SubjectRegistry createSubjectRegistry(ApplicationContext context, ObserverBeanDefinitionContext definitionContext,ObserverAsyncConfiguration asyncConfiguration) {
        return new SimpleSubjectRegistry(context,definitionContext,asyncConfiguration);
    }

    private ObserverRegistry createObserverRegistry(ApplicationContext context, ObserverBeanDefinitionContext definitionContext) {
        return new SimpleObserverRegistry(context,definitionContext);
    }
}
