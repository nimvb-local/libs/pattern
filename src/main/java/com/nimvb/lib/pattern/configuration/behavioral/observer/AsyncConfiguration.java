package com.nimvb.lib.pattern.configuration.behavioral.observer;

import com.nimvb.lib.pattern.behavioral.observer.async.ObserverAsyncConfiguration;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.task.TaskExecutionAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.task.TaskExecutor;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.AbstractAsyncConfiguration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.Executor;

@Configuration(proxyBeanMethods = false)
@ConditionalOnClass(EnableAsync.class)
@AutoConfigureAfter(TaskExecutionAutoConfiguration.class)
public class AsyncConfiguration extends AbstractAsyncConfiguration implements BeanFactoryAware {

    private BeanFactory beanFactory;

    @Bean
    ObserverAsyncConfiguration observerAsyncConfiguration(){

        if(this.executor == null || this.executor.get() == null){
            if(beanFactory != null){
                final Executor bean = beanFactory.getBean(Executor.class);
                return new ObserverAsyncConfiguration.ObserverAsyncConfigurationImpl(bean);
            }
            return null;
        }
        return new ObserverAsyncConfiguration.ObserverAsyncConfigurationImpl(this.executor.get());
    }

    @Override
    public void setBeanFactory(@Nullable BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }


}
