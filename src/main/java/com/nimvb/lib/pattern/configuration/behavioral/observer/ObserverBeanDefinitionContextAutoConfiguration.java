package com.nimvb.lib.pattern.configuration.behavioral.observer;


import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.property.ObserverConfigurationProperties;
import org.springframework.beans.factory.support.SimpleBeanDefinitionRegistry;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

@Configuration
@EnableConfigurationProperties(ObserverConfigurationProperties.class)
@ConditionalOnClass(ObserverBeanDefinitionContextAutoConfiguration.class)
@ComponentScan("com.nimvb.lib.pattern.behavioral.observer.*")
public class ObserverBeanDefinitionContextAutoConfiguration {


    @Bean
    ObserverBeanDefinitionContext observerBeanDefinitionContext(ApplicationContext context, ObserverConfigurationProperties properties){
        final ObserverBeanDefinitionContext.Builder builder = new ObserverBeanDefinitionContext.Builder(getCandidatePackages(context, properties), new SimpleBeanDefinitionRegistry());
        return builder.build();
    }

    private String[] getCandidatePackages(ApplicationContext context, ObserverConfigurationProperties properties){
        ArrayList<String> candidates = new ArrayList<>();
        final Map<String, Object> application = context.getBeansWithAnnotation(SpringBootApplication.class);
        if(!application.isEmpty()){
            final Optional<Object> candidate = application.values().stream().findFirst();
            candidate.ifPresent(o -> candidates.add(o.getClass().getPackageName()));
        }
        candidates.addAll(Arrays.asList(properties.getPackages()));
        return candidates.toArray(new String[0]);
    }
}
