package com.nimvb.lib.pattern.behavioral.observer.registry;

import com.nimvb.lib.pattern.behavioral.observer.DefaultSubject;
import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Subject;
import com.nimvb.lib.pattern.behavioral.observer.async.ObserverAsyncConfiguration;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.interceptor.SubjectInterceptor;
import com.nimvb.lib.pattern.behavioral.observer.registry.exception.EventTypeNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;

import java.util.AbstractMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RequiredArgsConstructor
public class SimpleSubjectRegistry implements SubjectRegistry{
    private final ApplicationContext context;
    private final ObserverBeanDefinitionContext definitions;
    private final ObserverAsyncConfiguration asyncConfiguration;
    Map<? super Class<? extends Event<?>>, Map.Entry<? super Class<Subject<?, ?>>,Subject<?,?>>> eventSubjectMap = new ConcurrentHashMap<>();

    public SimpleSubjectRegistry(ApplicationContext context,ObserverBeanDefinitionContext definitions){
        this.context = context;
        this.definitions = definitions;
        this.asyncConfiguration = null;
    }

    @Override
    public <TContext, TEvent extends Event<TContext>> Subject<TContext, TEvent> getSubjectOfEvent(@NonNull Class<TEvent> eventType) {
        if(definitions.getSubjectsMap().containsKey(eventType)){
            final Class<Subject<?, ?>> subjectClazz = (Class<Subject<?, ?>>)definitions.getSubjectsMap().get(eventType);
            if(exists(subjectClazz)){
                return (Subject<TContext, TEvent>)context.getBean(subjectClazz);
            }
            Subject<TContext, TEvent> subject = createSubject(subjectClazz);
            final String beanName = eventType.getName() + subjectClazz.getName();
            register(beanName,subject);
            return subject;
        }
        if(definitions.getEventsDefinition().contains(eventType)){
            if(eventSubjectMap.containsKey(eventType)){
                final Map.Entry<? super Class<Subject<?, ?>>, Subject<?, ?>> entry = eventSubjectMap.get(eventType);
                if(entry.getValue() != null){
                    return (Subject<TContext, TEvent>) entry.getValue();
                }

            }
            final Subject<?,?> subject = new DefaultSubject<>(asyncConfiguration);
            ProxyFactory factory = new ProxyFactory();
            factory.setProxyTargetClass(true);
            factory.setInterfaces(Subject.class);
            factory.setTarget(subject);
            factory.addAdvice(new SubjectInterceptor());
            final AbstractMap.SimpleEntry entry = new AbstractMap.SimpleEntry(subject.getClass(), factory.getProxy());
            eventSubjectMap.put(eventType, entry);
            return (Subject<TContext, TEvent>) entry.getValue();
        }
        throw new EventTypeNotFoundException();
    }

    private <TContext, TEvent extends Event<TContext>> void register(String name, Subject<TContext, TEvent> subject) {
        ((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerSingleton(name,subject);
    }

    private <TContext, TEvent extends Event<TContext>> Subject<TContext, TEvent> createSubject(Class<Subject<?, ?>> subjectClazz) {
        return (Subject<TContext, TEvent>) context.getAutowireCapableBeanFactory().createBean(subjectClazz, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR,true);
    }

    private boolean exists(Class<Subject<?, ?>> subjectClazz) {
        try {
            context.getBean(subjectClazz);
            return true;
        } catch (BeansException e) {
            return false;
        }
    }
}
