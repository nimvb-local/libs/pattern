package com.nimvb.lib.pattern.behavioral.observer.registry;

public interface RegistryProvider {
    ObserverRegistry getObserverRegistry();
    SubjectRegistry getSubjectRegistry();
}


