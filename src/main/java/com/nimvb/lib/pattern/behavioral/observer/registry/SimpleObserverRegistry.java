package com.nimvb.lib.pattern.behavioral.observer.registry;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Observer;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.exception.EventTypeNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class SimpleObserverRegistry implements ObserverRegistry {
    private final ApplicationContext context;
    private final ObserverBeanDefinitionContext definitions;

    @Override
    public <TContext, TEvent extends Event<TContext>> List<Observer<TContext, TEvent>> getObserversOfEvent(@NonNull Class<TEvent> event) {
        if(definitions.getObserversMap().containsKey(event)){
            final List<Class<Observer<?, ?>>> observersOfEvent = definitions.getObserversMap().get(event);

            final List<Observer<TContext, TEvent>> observers = observersOfEvent.stream().map(observerClass -> {
                if (exists(observerClass)) {
                    return (Observer<TContext, TEvent>) context.getBean(observerClass);
                }
                final Observer<TContext, TEvent> observer = createObserver(observerClass);
                register(observerClass.getName(), observer);
                return observer;
            }).collect(Collectors.toList());
            return observers;
        }
        throw new EventTypeNotFoundException();
    }

    private <TEvent extends Event<TContext>, TContext> void register(String name, Observer<TContext, TEvent> observer) {
        ((DefaultListableBeanFactory) context.getAutowireCapableBeanFactory()).registerSingleton(name,observer);
    }

    private <TEvent extends Event<TContext>, TContext> Observer<TContext, TEvent> createObserver(Class<Observer<?, ?>> observerClass) {
        return (Observer<TContext, TEvent>) context.getAutowireCapableBeanFactory().createBean(observerClass, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR,true);
    }

    private boolean exists(Class<Observer<?, ?>> observerClass) {
        try {
            context.getBean(observerClass);
            return true;
        } catch (BeansException e) {
            return false;
        }
    }
}
