package com.nimvb.lib.pattern.behavioral.observer.registry;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Observer;

import java.util.List;

public interface ObserverRegistry {

    <TContext,TEvent extends Event<TContext>> List<Observer<TContext,TEvent>> getObserversOfEvent(Class<TEvent> event);
}
