package com.nimvb.lib.pattern.behavioral.observer.bus;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.bus.EventBus;

public interface Connector {

    <TContext, TEvent extends Event<TContext>> EventBus build();

}
