package com.nimvb.lib.pattern.behavioral.chain.builder;


import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutorWithResult;
import com.nimvb.lib.pattern.behavioral.chain.layer.context.LayerContext;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;

public interface ExecutorBuilder {

    ExecutorBuilder add(Stage stage);

    //LayerExecutor build();
    ChainExecutor build();

    class ExecutorBuilderImpl implements ExecutorBuilder {

        private LayerContext starter = null;

        @Override
        public ExecutorBuilder add(Stage stage) {
            LayerContext node = new LayerContext(stage);
            if (starter == null) {
                starter = node;
            } else {
                LayerContext current = starter;
                while (current.getNext() != null) {
                    current = current.getNext();
                }
                current.setNext(node);
            }
            return this;
        }
//
//        @Override
//        public LayerExecutor build() {
//            return new LayerExecutorWithResult.LayerExecutorImpl(starter, null);
//        }

        @Override
        public ChainExecutor build() {
            LayerExecutorWithResult.LayerExecutorImpl<?> executor = new LayerExecutorWithResult.LayerExecutorImpl<>(starter);
            return new ChainExecutor.ChainExecutorImpl(executor);
        }
    }
}
