package com.nimvb.lib.pattern.behavioral.observer.discovery.stage;

import com.nimvb.lib.pattern.behavioral.chain.stage.AbstractStage;
import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Subject;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.GenericTypeResolver;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class SubjectScanningStage extends AbstractStage<DiscoveryContext> {

    @NonNull
    private final String[] packages;
    @NonNull
    private final BeanDefinitionRegistry registry;

    @Override
    protected void execute(DiscoveryContext content) {

        if (packages.length > 0) {
            ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry);
            scanner.resetFilters(false);
            scanner.setIncludeAnnotationConfig(false);
            scanner.addIncludeFilter(new AssignableTypeFilter(Subject.class));
            scanner.scan(packages);
            final String[] beans = registry.getBeanDefinitionNames();
            final Map<? super Class<? extends Event<?>>, Class<Subject<?, ?>>> subjectMap = Arrays.stream(beans)
                    .map(clazz -> {
                        Optional<? extends Class<Subject<?, ?>>> target = Optional.empty();
                        try {
                            final Class<Subject<?, ?>> type = (Class<Subject<?, ?>>) Class.forName(registry.getBeanDefinition(clazz).getBeanClassName());
                            target = Optional.of(type);
                        } catch (ClassNotFoundException exception) {
                            log.warn(exception.getMessage());
                        }
                        return target;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(clazz -> {
                        Optional<Map.Entry<Class<? super Event<?>>, Class<Subject<?, ?>>>> entry = Optional.empty();
                        final Class<?>[] generics = GenericTypeResolver.resolveTypeArguments(clazz, Subject.class);
                        if (generics != null && generics.length >= 2) {
                            if (generics[1] != null) {
                                final Class<? super Event<?>> eventType = (Class<? super Event<?>>) generics[1];
                                entry = Optional.of(new AbstractMap.SimpleEntry<>(eventType, clazz));
                            }
                        } else {
                            log.warn("skipping registration of the subject " + clazz.getCanonicalName() + " for no explicit event declaration");
                        }
                        return entry;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            content.setSubjectsMap(subjectMap);
        }

        next();
    }
}
