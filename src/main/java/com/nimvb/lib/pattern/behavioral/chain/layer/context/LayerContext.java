package com.nimvb.lib.pattern.behavioral.chain.layer.context;



import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@RequiredArgsConstructor
public class LayerContext {
    @Getter
    private final Stage stage;
    @Getter
    @Setter
    LayerContext next = null;
}
