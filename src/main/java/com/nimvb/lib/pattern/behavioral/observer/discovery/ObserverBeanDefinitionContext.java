package com.nimvb.lib.pattern.behavioral.observer.discovery;


import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Observer;
import com.nimvb.lib.pattern.behavioral.observer.Subject;
import com.nimvb.lib.pattern.behavioral.observer.discovery.stage.DiscoveryContext;
import com.nimvb.lib.pattern.behavioral.observer.discovery.stage.EventScanningStage;
import com.nimvb.lib.pattern.behavioral.observer.discovery.stage.ObserverScanningStage;
import com.nimvb.lib.pattern.behavioral.observer.discovery.stage.SubjectScanningStage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Value
public class ObserverBeanDefinitionContext {
    @NonNull Map<? extends Class<? extends Event<?>>, List<Class<Observer<?, ?>>>> observersMap;
    @NonNull Map<? super Class<? extends Event<?>>, ? super Class<Subject<?, ?>>> subjectsMap;
    @NonNull Collection<Class<? extends Event<?>>> eventsDefinition;


    @RequiredArgsConstructor
    public static class Builder implements ObserverBeanDefinitionContextBuilder {

        @NonNull
        private final String[] packages;
        @NonNull
        private final BeanDefinitionRegistry registry;

        @Override
        public ObserverBeanDefinitionContext build() {
            final StageContext context = new StageContext();
            context.setContent(new DiscoveryContext());
            final ChainExecutor chainExecutor = Chain.INSTANCE.builder()
                    .add(new EventScanningStage(packages,registry))
                    .add(new SubjectScanningStage(packages,registry))
                    .add(new ObserverScanningStage(packages,registry))
                    .build();
            final DiscoveryContext content = chainExecutor.execute(context).content();
            return new ObserverBeanDefinitionContext(content.getObserversMap(),content.getSubjectsMap(),content.getEvents());
        }
    }

}
