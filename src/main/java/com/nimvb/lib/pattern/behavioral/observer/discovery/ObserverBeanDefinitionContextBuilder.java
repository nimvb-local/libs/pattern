package com.nimvb.lib.pattern.behavioral.observer.discovery;

public interface ObserverBeanDefinitionContextBuilder {
    ObserverBeanDefinitionContext build();
}
