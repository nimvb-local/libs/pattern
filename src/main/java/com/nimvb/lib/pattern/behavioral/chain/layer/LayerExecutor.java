package com.nimvb.lib.pattern.behavioral.chain.layer;


import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;

public interface LayerExecutor {

    void next(StageContext context);
}
