package com.nimvb.lib.pattern.behavioral.chain.stage;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import org.springframework.core.GenericTypeResolver;
import org.springframework.util.Assert;

public abstract class AbstractStage<TContext> implements Stage {

    private LayerExecutor chain;
    private StageContext context;

    @Override
    public StageContext execute(StageContext context, LayerExecutor chain) {
        Assert.notNull(context,"stage context is null");
        Assert.notNull(context.getContent(),"stage content is null");
        final Class<?>[] generics = GenericTypeResolver.resolveTypeArguments(this.getClass(), AbstractStage.class);
        Assert.notNull(generics,"invalid generic parameters");
        Assert.isTrue(generics.length >= 1,"invalid type");
        Assert.notNull(generics[0],"generic parameter type is null");
        Assert.isInstanceOf(generics[0],context.getContent(),"stage content type is not valid");
        this.chain = chain;
        this.context = context;
        execute(context.getContent());
        return context;
    }

    protected void next(){
        chain.next(context);
    }

    protected abstract void execute(TContext content);
}
