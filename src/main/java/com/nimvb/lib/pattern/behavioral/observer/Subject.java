package com.nimvb.lib.pattern.behavioral.observer;

public interface Subject<TContext,TEvent extends Event<TContext>> {
    void add(Observer<TContext,TEvent> observer);
    void remove(Observer<TContext,TEvent> observer);
    void notify(TEvent event);
}
