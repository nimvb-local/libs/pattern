package com.nimvb.lib.pattern.behavioral.chain;

import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import lombok.RequiredArgsConstructor;

public interface ChainExecutionReport {

    StageContext result();

    <T> T content();

    @RequiredArgsConstructor
    public static class ChainExecutionReportImpl implements ChainExecutionReport {

        private final StageContext context;

        @Override
        public StageContext result() {
            return (StageContext) context;
        }

        @Override
        public <T> T content() {
            return result().getContent();
        }
    }
}
