package com.nimvb.lib.pattern.behavioral.chain.layer;


import com.nimvb.lib.pattern.behavioral.chain.layer.context.LayerContext;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import lombok.RequiredArgsConstructor;

public interface LayerExecutorWithResult extends LayerExecutor {

    StageContext context();

    @RequiredArgsConstructor
    class LayerExecutorImpl<T> implements LayerExecutorWithResult {


        private LayerContext current;
        private StageContext currentContext = null;

        public LayerExecutorImpl(LayerContext layerContext) {
            current = layerContext;
        }

        @Override
        public void next(StageContext context) {
            currentContext = context;
            if(current != null) {
                Stage stage = current.getStage();
                current = current.getNext();
                stage.execute(context, this);
            }
//            LayerContext<?> next = (LayerContext<?>) stage;
//            if (next != null) {
//                next.getStage().execute(context, this);
//            }
        }

        @Override
        public StageContext context() {
            return currentContext;
        }
    }
}
