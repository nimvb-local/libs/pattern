package com.nimvb.lib.pattern.behavioral.observer.discovery.stage;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Observer;
import com.nimvb.lib.pattern.behavioral.observer.Subject;
import lombok.Data;

import java.util.*;

@Data
public class DiscoveryContext {
    Map<? extends Class<? extends Event<?>>, List<Class<Observer<?, ?>>>> observersMap = new HashMap<>();
    Map<? super Class<? extends Event<?>>, ? super Class<Subject<?, ?>>> subjectsMap = new HashMap<>();
    Collection<Class<? extends Event<?>>> events = new HashSet<>();
}
