package com.nimvb.lib.pattern.behavioral.observer.async;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.concurrent.Executor;
import java.util.function.Supplier;


public interface ObserverAsyncConfiguration {
    Executor getExecutor();

    @RequiredArgsConstructor
    public static class ObserverAsyncConfigurationImpl implements ObserverAsyncConfiguration {

        private final Executor executor;

        @Override
        public Executor getExecutor() {
            return executor;
        }
    }
}
