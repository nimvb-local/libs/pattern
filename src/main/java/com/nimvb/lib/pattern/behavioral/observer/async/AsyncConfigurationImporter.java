package com.nimvb.lib.pattern.behavioral.observer.async;

import com.nimvb.lib.pattern.configuration.behavioral.observer.AsyncConfiguration;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;

public class AsyncConfigurationImporter implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        return new String[]{AsyncConfiguration.class.getCanonicalName()};
    }
}
