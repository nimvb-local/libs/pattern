package com.nimvb.lib.pattern.behavioral.observer.discovery.stage;


import com.nimvb.lib.pattern.behavioral.chain.stage.AbstractStage;
import com.nimvb.lib.pattern.behavioral.observer.Event;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class EventScanningStage extends AbstractStage<DiscoveryContext> {

    @NonNull
    private final String[] packages;
    @NonNull
    private final BeanDefinitionRegistry registry;

    @Override
    protected void execute(DiscoveryContext content) {

        if (packages.length > 0) {
            ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry);
            scanner.resetFilters(false);
            scanner.setIncludeAnnotationConfig(false);
            scanner.addIncludeFilter(new AssignableTypeFilter(Event.class));
            scanner.scan(packages);
            final String[] beans = registry.getBeanDefinitionNames();
            final Set<Class<? extends Event<?>>> events = Arrays.stream(beans)
                    .map(beanName -> {
                        Optional<? extends Class<Event<?>>> target = Optional.empty();
                        try {
                            final Class<Event<?>> type = (Class<Event<?>>) Class.forName(registry.getBeanDefinition(beanName).getBeanClassName());
                            target = Optional.of(type);
                        } catch (ClassNotFoundException exception) {
                            log.warn(exception.getMessage());
                        }
                        return target;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toSet());
            content.setEvents(events);
        }

        next();

    }
}
