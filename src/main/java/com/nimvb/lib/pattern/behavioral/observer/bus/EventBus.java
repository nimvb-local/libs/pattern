package com.nimvb.lib.pattern.behavioral.observer.bus;

import com.nimvb.lib.pattern.behavioral.observer.Event;

public interface EventBus {
    <TContext,TEvent extends Event<TContext>> void notify(TEvent event);
}
