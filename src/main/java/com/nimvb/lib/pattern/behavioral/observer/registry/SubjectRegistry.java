package com.nimvb.lib.pattern.behavioral.observer.registry;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Subject;

public interface SubjectRegistry {

    <TContext,TEvent extends Event<TContext>> Subject<TContext,TEvent> getSubjectOfEvent(Class<TEvent> eventType);
}
