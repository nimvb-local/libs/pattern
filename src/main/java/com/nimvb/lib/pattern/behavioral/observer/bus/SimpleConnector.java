package com.nimvb.lib.pattern.behavioral.observer.bus;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Observer;
import com.nimvb.lib.pattern.behavioral.observer.Subject;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.RegistryProvider;
import com.nimvb.lib.pattern.behavioral.observer.registry.exception.EventTypeNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class SimpleConnector implements Connector {
    @NonNull
    private final ObserverBeanDefinitionContext definitionContext;
    @NonNull
    private final RegistryProvider registry;

    @Override
    public <TContext, TEvent extends Event<TContext>> EventBus build() {
        definitionContext.getEventsDefinition().stream().forEach(eventClass -> {
            try {
                final Subject<TContext, TEvent> subject = (Subject<TContext, TEvent>) registry.getSubjectRegistry().getSubjectOfEvent((Class<TEvent>)eventClass);
                final List<Observer<TContext, TEvent>> observers = registry.getObserverRegistry().getObserversOfEvent((Class<TEvent>) eventClass);
                observers.forEach(subject::add);
            } catch (EventTypeNotFoundException ignore) {

            }
        });
        return new SimpleEventBus(registry,true);
    }
}
