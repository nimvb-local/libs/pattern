package com.nimvb.lib.pattern.behavioral.chain;

import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutorWithResult;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import lombok.RequiredArgsConstructor;

public interface ChainExecutor {
    ChainExecutionReport execute(StageContext context);

    @RequiredArgsConstructor
    class ChainExecutorImpl implements ChainExecutor {
        private final LayerExecutorWithResult layerExecutorWithResult;

        @Override
        public ChainExecutionReport execute(StageContext context) {
            layerExecutorWithResult.next(context);
            StageContext result = layerExecutorWithResult.context();
            return new ChainExecutionReport.ChainExecutionReportImpl(result);
        }
    }
}
