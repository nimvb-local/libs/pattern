package com.nimvb.lib.pattern.behavioral.observer;

import java.util.Optional;

public interface Event<TContext> {
    Optional<TContext> getContext();
}
