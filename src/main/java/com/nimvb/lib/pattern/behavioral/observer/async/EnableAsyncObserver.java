package com.nimvb.lib.pattern.behavioral.observer.async;


import com.nimvb.lib.pattern.configuration.behavioral.observer.AsyncConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;
import org.springframework.scheduling.annotation.EnableAsync;

import java.lang.annotation.*;

@EnableAsync
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(AsyncConfigurationImporter.class)
public @interface EnableAsyncObserver {
}
