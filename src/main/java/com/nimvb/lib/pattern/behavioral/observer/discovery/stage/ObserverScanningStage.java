package com.nimvb.lib.pattern.behavioral.observer.discovery.stage;

import com.nimvb.lib.pattern.behavioral.chain.stage.AbstractStage;
import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Observer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.GenericTypeResolver;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class ObserverScanningStage extends AbstractStage<DiscoveryContext> {

    @NonNull
    private final String[] packages;
    @NonNull
    private final BeanDefinitionRegistry registry;

    @Override
    protected void execute(DiscoveryContext content) {
        if (packages.length > 0) {
            ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry);
            scanner.resetFilters(false);
            scanner.setIncludeAnnotationConfig(false);
            scanner.addIncludeFilter(new AssignableTypeFilter(Observer.class));
            scanner.scan(packages);
            final String[] beans = registry.getBeanDefinitionNames();
            final Map<Class<? extends Event<?>>, List<Class<Observer<?, ?>>>> observerMap = Arrays.stream(beans)
                    .map(clazz -> {
                        Optional<? extends Class<Observer<?, ?>>> target = Optional.empty();
                        try {
                            final Class<Observer<?, ?>> type = (Class<Observer<?, ?>>) Class.forName(registry.getBeanDefinition(clazz).getBeanClassName());
                            target = Optional.of(type);
                        } catch (ClassNotFoundException exception) {
                            log.warn(exception.getMessage());
                        }
                        return target;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(clazz -> {
                        Optional<Map.Entry<Class<? extends Event<?>>, Class<Observer<?, ?>>>> entry = Optional.empty();
                        final Class<?>[] generics = GenericTypeResolver.resolveTypeArguments(clazz, Observer.class);
                        if (generics != null && generics.length >= 2) {
                            if (generics[1] != null) {
                                final Class<? extends Event<?>> eventType = (Class<? extends Event<?>>) generics[1];
                                entry = Optional.of(new AbstractMap.SimpleEntry<>(eventType, clazz));
                            }
                        } else {
                            log.warn("skipping registration of the observer " + clazz.getCanonicalName() + " for no explicit event declaration");
                        }
                        return entry;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(
                            Collectors.groupingBy(
                                    (Function<Map.Entry<Class<? extends Event<?>>, Class<Observer<?, ?>>>, Class<? extends Event<?>>>) Map.Entry::getKey
                                    ,
                                    Collectors.mapping(
                                            Map.Entry::getValue,
                                            Collectors.toList()
                                    )
                            )
                    );
            content.setObserversMap(observerMap);
        }
        next();
    }
}
