package com.nimvb.lib.pattern.behavioral.observer;

public interface Observer<TContext,TEvent extends Event<TContext>> {
    void observe(TEvent event);
}
