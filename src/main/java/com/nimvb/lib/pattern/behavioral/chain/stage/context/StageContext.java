package com.nimvb.lib.pattern.behavioral.chain.stage.context;

import lombok.Data;

@Data
public class StageContext {
    Object content;

    public <T> T getContent() {
        return (T) content;
    }
}
