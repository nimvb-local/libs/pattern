package com.nimvb.lib.pattern.behavioral.observer.registry;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DefaultRegistryProvider implements RegistryProvider{

    @NonNull private final ObserverRegistry observerRegistry;
    @NonNull private final SubjectRegistry subjectRegistry;

    @Override
    public ObserverRegistry getObserverRegistry() {
        return observerRegistry;
    }

    @Override
    public SubjectRegistry getSubjectRegistry() {
        return subjectRegistry;
    }
}
