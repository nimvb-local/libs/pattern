package com.nimvb.lib.pattern.behavioral.chain.stage;


import com.nimvb.lib.pattern.behavioral.chain.layer.LayerExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;

public interface Stage {
    StageContext execute(StageContext context, LayerExecutor chain);
}
