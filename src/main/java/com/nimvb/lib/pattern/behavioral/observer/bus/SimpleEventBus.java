package com.nimvb.lib.pattern.behavioral.observer.bus;

import com.nimvb.lib.pattern.behavioral.observer.Event;
import com.nimvb.lib.pattern.behavioral.observer.Observer;
import com.nimvb.lib.pattern.behavioral.observer.Subject;
import com.nimvb.lib.pattern.behavioral.observer.discovery.ObserverBeanDefinitionContext;
import com.nimvb.lib.pattern.behavioral.observer.registry.RegistryProvider;
import com.nimvb.lib.pattern.behavioral.observer.registry.exception.EventTypeNotFoundException;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;

public class SimpleEventBus implements EventBus {
    private final RegistryProvider registry;
    @Getter
    private final boolean connected;

    public SimpleEventBus(@NonNull RegistryProvider registry){
        this.registry = registry;
        this.connected = false;
    }

    public SimpleEventBus(@NonNull RegistryProvider registry, boolean connected) {
        this.registry = registry;
        this.connected = connected;
    }

    @Override
    public <TContext, TEvent extends Event<TContext>> void notify(TEvent event) {
        if (connected) {
            final Subject<TContext, TEvent> subjectOfEvent = (Subject<TContext, TEvent>) registry.getSubjectRegistry().getSubjectOfEvent(event.getClass());
            subjectOfEvent.notify(event);
        }
    }

}
