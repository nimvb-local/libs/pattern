package com.nimvb.lib.pattern.behavioral.observer;

import com.nimvb.lib.pattern.behavioral.observer.async.AsyncObserver;
import com.nimvb.lib.pattern.behavioral.observer.async.ObserverAsyncConfiguration;
import org.springframework.aop.support.AopUtils;
import org.springframework.lang.Nullable;
import org.springframework.scheduling.annotation.Async;

import java.lang.annotation.Annotation;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class DefaultSubject<TContext,TEvent extends Event<TContext>> implements Subject<TContext,TEvent>{

    private final Set<Observer<TContext,TEvent>> observers = new CopyOnWriteArraySet<>();
    private final Optional<ObserverAsyncConfiguration> asyncConfiguration;

    public DefaultSubject(@Nullable ObserverAsyncConfiguration asyncConfiguration) {
        this.asyncConfiguration = Optional.ofNullable(asyncConfiguration);
    }


    @Override
    public void add(Observer<TContext, TEvent> observer) {
        observers.add(observer);
    }

    @Override
    public void remove(Observer<TContext, TEvent> observer) {
        observers.remove(observer);
    }

    @Override
    public void notify(TEvent event) {
        observers.forEach(observer -> {
            if(isAsyncExecutionEnabledFor(observer,event)){
                asyncConfiguration.get().getExecutor().execute(() -> observer.observe(event));
            }else {
                observer.observe(event);
            }
        });
    }

    boolean isAsyncExecutionEnabledFor(Observer<TContext, TEvent> observer, TEvent event){
        if(asyncConfiguration.isEmpty()){
            return false;
        }
        if(asyncConfiguration.get().getExecutor() == null){
            return false;
        }
        Class<?> observerClazz = null;
        final Observer<TContext, TEvent> observer1 = observer;
        if(AopUtils.isAopProxy(observer1)){
            observerClazz = AopUtils.getTargetClass(observer);
        }else {
            observerClazz = observer.getClass();
        }
        try {
            final Annotation[] annotations = observerClazz.getMethod("observe", event.getClass()).getAnnotations();
            for (Annotation annotation : annotations) {
                if(annotation.annotationType().equals(AsyncObserver.class)){
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
