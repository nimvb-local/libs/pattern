package com.nimvb.lib.pattern.behavioral.chain;

import com.nimvb.lib.pattern.behavioral.chain.builder.ExecutorBuilder;

public interface Chain {

    static Chain INSTANCE = new ChainImpl();

    ExecutorBuilder builder();

    public class ChainImpl implements Chain {

        @Override
        public ExecutorBuilder builder() {
            return new ExecutorBuilder.ExecutorBuilderImpl();
        }
    }
}
