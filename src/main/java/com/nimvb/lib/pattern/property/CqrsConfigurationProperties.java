package com.nimvb.lib.pattern.property;


import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties("spring.patterns.cqrs")
public class CqrsConfigurationProperties {

    private String[] packages = new String[0];

    public String[] getPackages() {
        return this.packages;
    }

    public void setPackages(String[] packages) {
        this.packages = packages;
    }
}
