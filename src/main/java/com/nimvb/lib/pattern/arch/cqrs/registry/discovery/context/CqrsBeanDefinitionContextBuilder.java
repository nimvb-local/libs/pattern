package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context;

public interface CqrsBeanDefinitionContextBuilder {
    CqrsBeansDefinitionContext build();


}
