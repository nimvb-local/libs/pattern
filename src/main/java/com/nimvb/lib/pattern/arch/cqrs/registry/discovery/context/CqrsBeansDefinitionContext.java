package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;
import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;
import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.stage.CommandExecutorsScanningStage;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.stage.QueryExecutorsScanningStage;
import com.nimvb.lib.pattern.behavioral.chain.Chain;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutionReport;
import com.nimvb.lib.pattern.behavioral.chain.ChainExecutor;
import com.nimvb.lib.pattern.behavioral.chain.stage.context.StageContext;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Getter
public class CqrsBeansDefinitionContext {

    @NonNull private final Map<? extends Class<? extends Command<?>>, Class<CommandExecutor<?, ?>>> commandExecutorMap;
    @NonNull private final Map<? extends Class<? extends Query<?>>, Class<QueryExecutor<?, ?>>> queryExecutorMap;

    public CqrsBeansDefinitionContext(){
        commandExecutorMap = new HashMap<>();
        queryExecutorMap = new HashMap<>();
    }

    @RequiredArgsConstructor
    public static class Builder implements CqrsBeanDefinitionContextBuilder {

        @NonNull
        private final String[] packages;
        @NonNull
        private final BeanDefinitionRegistry registry;

        @Override
        public CqrsBeansDefinitionContext build() {
            final StageContext context = new StageContext();
            context.setContent(new DiscoveryContext());
            final ChainExecutor chainExecutor = Chain.INSTANCE.builder()
                    .add(new CommandExecutorsScanningStage(packages, registry))
                    .add(new QueryExecutorsScanningStage(packages, registry))
                    .build();
            final ChainExecutionReport report = chainExecutor.execute(context);
            final DiscoveryContext discoveryContext = report.content();
            return (new CqrsBeansDefinitionContext(
                    discoveryContext.getCommandExecutorMap(),
                    discoveryContext.getQueryExecutorMap()
            ));
        }
    }
}
