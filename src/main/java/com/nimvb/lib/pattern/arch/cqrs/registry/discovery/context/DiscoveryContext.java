package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;
import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;
import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;
import lombok.Data;
import lombok.NonNull;

import java.util.HashMap;
import java.util.Map;

@Data
public class DiscoveryContext {

    private Map<? extends Class<? extends Command<?>>, Class<CommandExecutor<?, ?>>> commandExecutorMap = new HashMap<>();
    private Map<? extends Class<? extends Query<?>>, Class<QueryExecutor<?, ?>>> queryExecutorMap = new HashMap<>();

    public void setCommandExecutorMap(@NonNull Map<? extends Class<? extends Command<?>>, Class<CommandExecutor<?, ?>>> commandExecutorMap) {
        this.commandExecutorMap = commandExecutorMap;
    }

    public void setQueryExecutorMap(@NonNull Map<? extends Class<? extends Query<?>>, Class<QueryExecutor<?, ?>>> queryExecutorMap) {
        this.queryExecutorMap = queryExecutorMap;
    }
}
