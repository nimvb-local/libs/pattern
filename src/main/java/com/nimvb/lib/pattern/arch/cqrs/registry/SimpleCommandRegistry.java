package com.nimvb.lib.pattern.arch.cqrs.registry;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;
import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.CqrsBeansDefinitionContext;
import com.nimvb.lib.pattern.arch.cqrs.registry.exception.CommandExecutorNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;

@RequiredArgsConstructor
public class SimpleCommandRegistry implements CommandRegistry{

    private final ApplicationContext applicationContext;
    private final CqrsBeansDefinitionContext context;

    @Override
    public <TResult, TCommand extends Command<TResult>> CommandExecutor<TResult, TCommand> getExecutor(@NonNull  Class<TCommand> command) {
        if(context.getCommandExecutorMap().containsKey(command)){
            final Class<CommandExecutor<?, ?>> executorType = context.getCommandExecutorMap().get(command);
            if(exists(executorType)){
                return (CommandExecutor<TResult, TCommand>) applicationContext.getBean(executorType);
            }
            final CommandExecutor<TResult, TCommand> executor = (CommandExecutor<TResult, TCommand>) createExecutor(executorType);
            registerExecutor(executorType.getName(),executor);
            return executor;
        }
        throw new CommandExecutorNotFoundException();
    }

    private Object createExecutor(Class<CommandExecutor<?,?>> clazz){
        return applicationContext.getAutowireCapableBeanFactory().createBean(clazz,AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR,true);
    }

    private <TResult, TCommand extends Command<TResult>> void registerExecutor(String name,CommandExecutor<TResult, TCommand> instance){
        ((DefaultListableBeanFactory) applicationContext.getAutowireCapableBeanFactory()).registerSingleton(name,instance);
        setScope(name, ConfigurableBeanFactory.SCOPE_SINGLETON);
    }

    private boolean exists(Class<CommandExecutor<?,?>> clazz){
        try {
            this.applicationContext.getBean(clazz);
        } catch (BeansException e) {
            return false;
        }
        return true;
    }

    private void setScope(String beanName,String scope){
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) applicationContext.getAutowireCapableBeanFactory();
        if(registry.containsBeanDefinition(beanName)){
            final BeanDefinition definition = registry.getBeanDefinition(beanName);
            definition.setScope(scope);
            registry.registerBeanDefinition(beanName,definition);
        }
    }
}
