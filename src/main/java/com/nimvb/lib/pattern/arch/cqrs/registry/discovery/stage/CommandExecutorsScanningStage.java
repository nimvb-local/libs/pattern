package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.stage;


import com.nimvb.lib.pattern.arch.cqrs.command.Command;
import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.DiscoveryContext;
import com.nimvb.lib.pattern.behavioral.chain.stage.AbstractStage;
import com.nimvb.lib.pattern.behavioral.chain.stage.Stage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.GenericTypeResolver;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class CommandExecutorsScanningStage extends AbstractStage<DiscoveryContext> implements Stage {

    @NonNull
    private final String[] packages;
    @NonNull
    private final BeanDefinitionRegistry registry;


    @Override
    protected void execute(DiscoveryContext content) {
        if (packages.length > 0) {
            ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry);
            scanner.resetFilters(false);
            scanner.setIncludeAnnotationConfig(false);
            scanner.addIncludeFilter(new AssignableTypeFilter(CommandExecutor.class));
            scanner.scan(packages);
            final String[] beans = registry.getBeanDefinitionNames();
            final Map<? extends Class<? extends Command<?>>, Class<CommandExecutor<?, ?>>> commandExecutorMap = Arrays.stream(beans)
                    .map(clazz -> {
                        Optional<? extends Class<CommandExecutor<?, ?>>> target = Optional.empty();
                        try {
                            final Class<CommandExecutor<?, ?>> type = (Class<CommandExecutor<?, ?>>) Class.forName(registry.getBeanDefinition(clazz).getBeanClassName());
                            target = Optional.of(type);
                        } catch (ClassNotFoundException exception) {
                            log.warn(exception.getMessage());
                        }
                        return target;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(clazz -> {
                        Optional<Map.Entry<Class<? extends Command<?>>, Class<CommandExecutor<?, ?>>>> entry = Optional.empty();
                        final Class<?>[] generics = GenericTypeResolver.resolveTypeArguments(clazz, CommandExecutor.class);
                        if (generics != null && generics.length >= 2) {
                            if (generics[1] != null) {
                                final Class<? extends Command<?>> commandType = (Class<? extends Command<?>>) generics[1];
                                entry = Optional.of(new AbstractMap.SimpleEntry<>(commandType, clazz));
                            }
                        } else {
                            log.warn("skipping registration of the command executor " + clazz.getCanonicalName() + " for no explicit command declaration");
                        }
                        return entry;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            content.setCommandExecutorMap(commandExecutorMap);
        }
        next();
    }
}
