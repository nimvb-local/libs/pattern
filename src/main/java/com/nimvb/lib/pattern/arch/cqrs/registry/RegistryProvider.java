package com.nimvb.lib.pattern.arch.cqrs.registry;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public interface RegistryProvider {

    CommandRegistry getCommandRegistry();
    QueryRegistry getQueryRegistry();

    @RequiredArgsConstructor
    class DefaultRegistryProvider implements RegistryProvider {

        @NonNull private final CommandRegistry commandRegistry;
        @NonNull private final QueryRegistry queryRegistry;

        @Override
        public CommandRegistry getCommandRegistry() {
            return commandRegistry;
        }

        @Override
        public QueryRegistry getQueryRegistry() {
            return queryRegistry;
        }
    }
}
