package com.nimvb.lib.pattern.arch.cqrs.registry;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;
import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;

public interface CommandRegistry {
    <TResult, TCommand extends Command<TResult>> CommandExecutor<TResult, TCommand> getExecutor(Class<TCommand> command);
}
