package com.nimvb.lib.pattern.arch.cqrs.command;

public interface CommandExecutor<TResult, TCommand extends Command<TResult>> {

    TResult execute(TCommand command);
}
