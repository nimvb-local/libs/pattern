package com.nimvb.lib.pattern.arch.cqrs.bus;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;
import com.nimvb.lib.pattern.arch.cqrs.command.CommandExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.CommandRegistry;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SimpleCommandBus implements CommandBus{

    @NonNull private final CommandRegistry registry;

    @Override
    public <TResult, TCommand extends Command<TResult>> TResult execute(TCommand command) {
        final CommandExecutor<TResult, TCommand> executor = (CommandExecutor<TResult, TCommand>)registry.getExecutor(command.getClass());
        return executor.execute(command);
    }
}
