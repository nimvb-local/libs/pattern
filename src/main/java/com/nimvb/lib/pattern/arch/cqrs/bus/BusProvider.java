package com.nimvb.lib.pattern.arch.cqrs.bus;

import com.nimvb.lib.pattern.arch.cqrs.registry.RegistryProvider;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

public interface BusProvider {

    CommandBus getCommandBus();

    QueryBus getQueryBus();

    @RequiredArgsConstructor
    public class DefaultBusProvider implements BusProvider {

        @NonNull private final RegistryProvider registry;

        @Override
        public CommandBus getCommandBus() {
            return new SimpleCommandBus(registry.getCommandRegistry());
        }

        @Override
        public QueryBus getQueryBus() {
            return new SimpleQueryBus(registry.getQueryRegistry());
        }
    }
}
