package com.nimvb.lib.pattern.arch.cqrs.bus;

import com.nimvb.lib.pattern.arch.cqrs.query.Query;

public interface QueryBus {
    <TResult, TQuery extends Query<TResult>> TResult execute(TQuery query);
}
