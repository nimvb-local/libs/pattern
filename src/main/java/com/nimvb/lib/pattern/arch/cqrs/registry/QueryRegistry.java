package com.nimvb.lib.pattern.arch.cqrs.registry;

import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;

public interface QueryRegistry {

    <TResult,TQuery extends Query<TResult>> QueryExecutor<TResult,TQuery> getExecutor(Class<TQuery> query);
}
