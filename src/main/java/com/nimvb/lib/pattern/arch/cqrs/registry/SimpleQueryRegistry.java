package com.nimvb.lib.pattern.arch.cqrs.registry;

import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.CqrsBeansDefinitionContext;
import com.nimvb.lib.pattern.arch.cqrs.registry.exception.QueryExecutorNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;

@RequiredArgsConstructor
public class SimpleQueryRegistry  implements QueryRegistry{

    private final ApplicationContext applicationContext;
    private final CqrsBeansDefinitionContext context;
    @Override
    public <TResult, TQuery extends Query<TResult>> QueryExecutor<TResult, TQuery> getExecutor(@NonNull Class<TQuery> query) {
        if(context.getQueryExecutorMap().containsKey(query)){
            final Class<QueryExecutor<?, ?>> executorType = context.getQueryExecutorMap().get(query);
            if(exists(executorType)){
                return (QueryExecutor<TResult, TQuery>) applicationContext.getBean(executorType);
            }
            final QueryExecutor<TResult, TQuery> executor = (QueryExecutor<TResult, TQuery>) createExecutor(executorType);
            registerExecutor(executorType.getName(),executor);
            return executor;
        }
        throw new QueryExecutorNotFoundException();
    }


    private Object createExecutor(Class<QueryExecutor<?,?>> clazz){
        return applicationContext.getAutowireCapableBeanFactory().createBean(clazz, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR,true);
    }

    private <TResult, TQuery extends Query<TResult>> void registerExecutor(String name, QueryExecutor<TResult, TQuery> instance){
        ((DefaultListableBeanFactory) applicationContext.getAutowireCapableBeanFactory()).registerSingleton(name,instance);
        setScope(name, ConfigurableBeanFactory.SCOPE_SINGLETON);
    }

    private boolean exists(Class<QueryExecutor<?,?>> clazz){
        try {
            this.applicationContext.getBean(clazz);
        } catch (BeansException e) {
            return false;
        }
        return true;
    }

    private void setScope(String beanName,String scope){
        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) applicationContext.getAutowireCapableBeanFactory();
        if(registry.containsBeanDefinition(beanName)){
            final BeanDefinition definition = registry.getBeanDefinition(beanName);
            definition.setScope(scope);
            registry.registerBeanDefinition(beanName,definition);
        }
    }
}
