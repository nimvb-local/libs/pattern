package com.nimvb.lib.pattern.arch.cqrs.bus;

import com.nimvb.lib.pattern.arch.cqrs.command.Command;

public interface CommandBus {

    <TResult, TCommand extends Command<TResult>> TResult execute(TCommand command);
}
