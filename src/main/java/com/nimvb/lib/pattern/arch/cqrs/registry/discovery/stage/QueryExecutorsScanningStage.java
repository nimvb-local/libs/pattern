package com.nimvb.lib.pattern.arch.cqrs.registry.discovery.stage;


import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.discovery.context.DiscoveryContext;
import com.nimvb.lib.pattern.behavioral.chain.stage.AbstractStage;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.GenericTypeResolver;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@Slf4j
@RequiredArgsConstructor
public class QueryExecutorsScanningStage extends AbstractStage<DiscoveryContext> {

    @NonNull
    private final String[] packages;
    @NonNull
    private final BeanDefinitionRegistry registry;


    @Override
    protected void execute(DiscoveryContext content) {
        if (packages.length > 0) {
            ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(registry);
            scanner.resetFilters(false);
            scanner.setIncludeAnnotationConfig(false);
            scanner.addIncludeFilter(new AssignableTypeFilter(QueryExecutor.class));
            scanner.scan(packages);
            final String[] beans = registry.getBeanDefinitionNames();
            final Map<? extends Class<? extends Query<?>>, Class<QueryExecutor<?, ?>>> queryExecutorMap = Arrays.stream(beans)
                    .map(clazz -> {
                        Optional<? extends Class<QueryExecutor<?, ?>>> target = Optional.empty();
                        try {
                            final Class<QueryExecutor<?, ?>> type = (Class<QueryExecutor<?, ?>>) Class.forName(registry.getBeanDefinition(clazz).getBeanClassName());
                            target = Optional.of(type);
                        } catch (ClassNotFoundException exception) {
                            log.warn(exception.getMessage());
                        }
                        return target;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .map(clazz -> {
                        Optional<Map.Entry<Class<? extends Query<?>>, Class<QueryExecutor<?, ?>>>> entry = Optional.empty();
                        final Class<?>[] generics = GenericTypeResolver.resolveTypeArguments(clazz, QueryExecutor.class);
                        if (generics != null && generics.length >= 2) {
                            if (generics[1] != null) {
                                final Class<? extends Query<?>> queryType = (Class<? extends Query<?>>) generics[1];
                                entry = Optional.of(new AbstractMap.SimpleEntry<>(queryType, clazz));
                            }
                        } else {
                            log.warn("skipping registration of the query executor " + clazz.getCanonicalName() + " for no explicit query declaration");
                        }
                        return entry;
                    })
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            content.setQueryExecutorMap(queryExecutorMap);
        }
    }
}
