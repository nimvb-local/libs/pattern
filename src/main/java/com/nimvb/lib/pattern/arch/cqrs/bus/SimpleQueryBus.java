package com.nimvb.lib.pattern.arch.cqrs.bus;

import com.nimvb.lib.pattern.arch.cqrs.query.Query;
import com.nimvb.lib.pattern.arch.cqrs.query.QueryExecutor;
import com.nimvb.lib.pattern.arch.cqrs.registry.QueryRegistry;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SimpleQueryBus implements QueryBus{

    @NonNull private final QueryRegistry registry;

    @Override
    public <TResult, TQuery extends Query<TResult>> TResult execute(TQuery query) {
        final QueryExecutor<TResult, TQuery> executor = (QueryExecutor<TResult, TQuery>) registry.getExecutor(query.getClass());
        return executor.execute(query);
    }
}
