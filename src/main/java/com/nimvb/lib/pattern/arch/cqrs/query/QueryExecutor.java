package com.nimvb.lib.pattern.arch.cqrs.query;

public interface QueryExecutor<TResult,TQuery extends Query<TResult>> {

    TResult execute(TQuery query);
}
